package com.te

import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.kotest.annotation.MicronautTest

@MicronautTest
class HelloControllerBehaviorSpec(@Client("/") private val client: HttpClient) : BehaviorSpec({
    given("a hello endpoint") {
        `when`("we hit /hello") {
            val resp: String = client.toBlocking().retrieve("/hello")
            then("we should get back plain text with a spanish greeting") {
                resp shouldBe "Hola!"
            }
        }
    }
})
