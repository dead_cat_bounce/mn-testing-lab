package com.te

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.kotest.annotation.MicronautTest
import javax.inject.Inject

@MicronautTest
class HelloControllerAnnotationTest : AnnotationSpec() {
    @Inject
    @field:Client("/")
    lateinit var client: RxHttpClient

    @Test
    fun anAnnotationSpecTest() {
        val resp: String = client.toBlocking().retrieve("/hello")
          resp shouldBe "Hola!"
    }
}
