package com.te

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.micronaut.http.client.RxHttpClient
import io.micronaut.test.extensions.kotest.annotation.MicronautTest
import io.micronaut.http.client.annotation.Client

@MicronautTest
class HelloControllerStringSpec(@Client("/") private val client: RxHttpClient) : StringSpec({
    "a string spec" {
        val resp: String = client.toBlocking().retrieve("/hello")
        resp shouldBe "Hola!"
    }
})
